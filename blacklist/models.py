from django.db import models

from allianceauth.authentication.models import User
from allianceauth.eveonline.models import EveCharacter


def get_sentinel_user():
    """
    Get or create the "deleted" user.
    :return:
    """
    return User.objects.get_or_create(name="Deleted User")[0]


# Create your models here.
class Entry(models.Model):
    character = models.ForeignKey(EveCharacter, on_delete=models.DO_NOTHING)
    reason = models.TextField(max_length=2000)
    agent = models.ForeignKey(User, on_delete=models.SET(get_sentinel_user))

    def __str__(self):
        return f'Entry for: {self.character.name} (Created by: {self.agent.name})'

    def __repr__(self):
        return f'<{self.__str__()}>'


class Comment(models.Model):
    text = models.TextField()
    author = models.ForeignKey(User, on_delete=models.SET(get_sentinel_user))
    created_date = models.DateTimeField(auto_now_add=True)
    entry = models.ForeignKey(Entry, related_name='comments', on_delete=models.CASCADE)

    def __str__(self):
        return f'Comment on: {self.entry.pk} (By: {self.author.name})'

    def __repr__(self):
        return f'<{self.__str__()}>'
